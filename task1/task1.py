import urllib2
import json

_DB_URL = 'http://www.omdbapi.com/?s={}'

def retrieve_movie_information(substring):
    request = urllib2.Request(_DB_URL.format(substring))
    try:
        raw_result = urllib2.urlopen(request).read()
    except urllib2.HTTPError:
        print 'Error querying movie databse endpoint.'
        return

    json_result = json.loads(raw_result)
    if json_result['Response'] == 'False':
        print 'No movies found with substring {} in title.'.format(substring)
    else:
        print json_result['totalResults']


while True:
    substring = raw_input('Enter Search Term: ')
    if substring:
        retrieve_movie_information(substring)
