_MONTH_TO_NUMBER_MAP = {
        'Jan':'01',
        'Feb':'02',
        'Mar':'03',
        'Apr':'04',
        'May':'05',
        'Jun':'06',
        'Jul':'07',
        'Aug':'08',
        'Sep':'09',
        'Oct':'10',
        'Nov':'11',
        'Dec':'12'
}

_DATE_FORMAT = '{}-{}-{}'

def reformatDate(date):
    parts = date.split()
    assert(len(parts) == 3)

    # Add a leading zero if it is a single digit day of the month, and strip the
    # ordinal indicator from the day.
    day = '0'*(4-len(parts[0])) + parts[0][:-2]
    return _DATE_FORMAT.format(parts[2], _MONTH_TO_NUMBER_MAP[parts[1]], day)


results = []
num_cases = int(raw_input("Number of testcases: "))
for i in xrange(num_cases):
    results.append(reformatDate(raw_input()))
for result in results:
    print result
